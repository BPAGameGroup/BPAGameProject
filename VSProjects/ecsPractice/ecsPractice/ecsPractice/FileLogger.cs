﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ecsPractice
{
    public class FileLogger : ILogger, IDisposable
    {
        private readonly string _logFilePath;
        private readonly FileStream _logStream;
        private readonly StreamWriter _logStreamWriter;

        public FileLogger(string path)
        {
            this._logFilePath = path;
            this._logStream = new FileStream(this._logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            this._logStreamWriter = new StreamWriter(this._logStream);
        }

        public void Dispose()
        {
            this._logStreamWriter.Close();
            this._logStreamWriter.Dispose();

            this._logStream.Close();
            this._logStream.Dispose();
        }

        public void Log(string text)
        {
            this.Log(text, DateTime.UtcNow);
        }
        
        public void Log(string text, DateTime time)
        {
            string timeString = time.ToString("yyyy-MM-dd HH:mm:ss");

            this._logStreamWriter.WriteLine(String.Format("[NORM] {0} - {1}", timeString, text));
        }

        public void LogError(string text)
        {
            this.LogError(text, DateTime.UtcNow);
        }

        public void LogError(string text, DateTime time)
        {
            string timeString = time.ToString("yyyy-MM-dd HH:mm:ss");

            this._logStreamWriter.WriteLine(String.Format("[ERROR] {0} - {1}", timeString, text));
        }
    }
}
