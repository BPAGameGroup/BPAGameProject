﻿using ecsPractice.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice
{
    public class EcsEntityWrapper
    {
        public int ID
        {
            get { return Components[0].EntityID; }
        }
        public List<EcsComponent> Components { get; private set; }

        public EcsEntityWrapper(int id, params EcsComponent[] components)
        {
            this.Components = new List<EcsComponent>(components);
        }
    }
}
