﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice
{
    public struct CollisionEvent
    {
        public int Entity1Id { get; set; }
        public int Entity2Id { get; set; }

        public CollisionEvent(int e1Id, int e2Id)
        {
            this.Entity1Id = e1Id;
            this.Entity2Id = e2Id;
        }
    }
}
