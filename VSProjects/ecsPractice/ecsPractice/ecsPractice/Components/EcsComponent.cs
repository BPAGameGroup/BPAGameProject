﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public abstract class EcsComponent
    {
        public EntityManager Parent;
        public int EntityID { get; set; }

        public EcsComponent(EntityManager parent)
        {
            this.Parent = parent;
        }

        public EcsComponent(int entityId)
        {
            this.EntityID = entityId;
        }
    }
}
