﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public class MovementIntentsComponent : EcsComponent
    {
        public bool LeftIntent { get; set; }
        public bool RightIntent { get; set; }
        public bool JumpIntent { get; set; }

        public MovementIntentsComponent(EntityManager parent)
            : base(parent)
        {
            this.ResetIntents();
        }

        public void ResetIntents()
        {
            this.LeftIntent = false;
            this.RightIntent = false;
            this.JumpIntent = false;
        }
    }
}
