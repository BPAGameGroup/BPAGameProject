﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public class MovableComponent : EcsComponent
    {
        public Vector2 Velocity { get; set; }

        public MovableComponent(EntityManager parent, Vector2 velocity)
            : base(parent)
        {
            this.Velocity = velocity;
        }
    }
}
