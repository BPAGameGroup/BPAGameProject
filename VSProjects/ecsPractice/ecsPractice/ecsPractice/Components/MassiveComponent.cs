﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public class MassiveComponent : EcsComponent
    {
        public float Mass { get; set; }

        public MassiveComponent(EntityManager parent, float mass)
            : base(parent)
        {
            this.Mass = mass;
        }
    }
}
