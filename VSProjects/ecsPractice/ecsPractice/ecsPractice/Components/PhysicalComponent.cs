﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public class PhysicalComponent : EcsComponent
    {
        public Vector2 Position { get; set; }

        public PhysicalComponent(EntityManager parent, Vector2 position)
            : base(parent)
        {
            this.Position = position;
        }
    }
}
