﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public class CollideableComponent : EcsComponent
    {
        public (int Width, int Height) BoundingBox { get; set; }

        public CollideableComponent(EntityManager parent, (int width, int height) bounds)
            : base(parent)
        {
            this.BoundingBox = bounds;
        }
    }
}
