﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Components
{
    public class RenderableComponent : EcsComponent
    {
        public Texture2D Texture { get; set; }

        public RenderableComponent(EntityManager parent, Texture2D texture)
            : base(parent)
        {
            this.Texture = texture;
        }
    }
}
