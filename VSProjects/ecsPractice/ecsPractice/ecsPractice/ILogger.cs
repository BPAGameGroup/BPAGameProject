﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice
{
    public interface ILogger
    {
        void Log(string text);

        void Log(string text, DateTime time);

        void LogError(string text);

        void LogError(string text, DateTime time);
    }
}
