﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ecsPractice.Components;

namespace ecsPractice
{
    public class EntityManager
    {
        public EcsPracticeGame Parent { get; set; }
        public List<EcsComponent> Components = new List<EcsComponent>();

        public EntityManager(EcsPracticeGame parent)
        {
            this.Parent = parent;
        }

        public void AddComponent(EcsComponent component)
        {
            component.Parent = this;
            this.Components.Add(component);
        }

        public void RemoveComponent(EcsComponent component)
        {
            component.Parent = null;
            this.Components.Remove(component);
        }

        public void AddEntity(params EcsComponent[] components)
        {
            int id;
            if (this.Components.Count == 0)
                { id = 0; }
            else
                { id = this.Components.Max(component => component.EntityID) + 1; }

            foreach (EcsComponent component in components)
            {
                component.EntityID = id;
                this.Components.Add(component);
            }
        }

        public EcsComponent[] GetEntity(int id)
        {
            if (id < 0)
                { throw new ArgumentException("Given ID does not map to a valid entity.", nameof(id)); }

            IEnumerable<EcsComponent> components = this.Components.Where(comp => comp.EntityID == id);

            if (!components.Any())
                { throw new ArgumentException("Given ID does not map to a valid entity.", nameof(id)); }

            return components.ToArray();
        }
    }
}
