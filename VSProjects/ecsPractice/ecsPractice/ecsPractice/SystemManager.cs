﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ecsPractice.Systems;
using Microsoft.Xna.Framework.Graphics;

namespace ecsPractice
{
    public class SystemManager
    {
        private static readonly Dictionary<EcsSystem.SYSTEM_TYPE, int> SYSTEM_EXECUTION_ORDER = new Dictionary<EcsSystem.SYSTEM_TYPE, int>()
        {
            { EcsSystem.SYSTEM_TYPE.Background, 0 },
            { EcsSystem.SYSTEM_TYPE.Gameplay, 1 },
            { EcsSystem.SYSTEM_TYPE.UI, 1 },
            { EcsSystem.SYSTEM_TYPE.Draw, 2 },
        };

        public EcsPracticeGame Parent { get; set; }
        public List<EcsSystem> Systems = new List<EcsSystem>();
        public List<CollisionEvent> CollisionEvents = new List<CollisionEvent>();

        public SystemManager(EcsPracticeGame parent)
        {
            this.Parent = parent;
        }

        public void Update()
        {
            IEnumerable<IGrouping<EcsSystem.SYSTEM_TYPE, EcsSystem>> grouped = this.Systems.GroupBy(sys => sys.Type, sys => sys);
            IOrderedEnumerable<IGrouping<EcsSystem.SYSTEM_TYPE, EcsSystem>> ordered = grouped.OrderBy(sys => SYSTEM_EXECUTION_ORDER[sys.Key]);

            foreach (IGrouping<EcsSystem.SYSTEM_TYPE, EcsSystem> type in ordered)
            {
                List<EcsSystem> systems = type.ToList();
                List<EcsSystem> orderedSystems = systems.OrderBy(sys => sys.ExecutionOrder).ToList();

                foreach (EcsSystem system in orderedSystems)
                {
                    system.Update();
                }
            }

            //foreach (EcsSystem s in this.Systems)
            //{
            //    s.Update();
            //}

            // Sorts by execution order so that, for example, InputSystem is updated before MovementIntentSystem.
            //IEnumerable<EcsSystem> sortedByExecutionOrder = this.Systems.OrderBy<EcsSystem, int>(system => system.ExecutionOrder);
            //foreach (EcsSystem s in this.Systems)
            //{
            //    s.Update();
            //}
        }

        public void Add(EcsSystem system)
        {
            system.Parent = this;
            this.Systems.Add(system);
        }

        public void Remove(EcsSystem system)
        {
            system.Parent = null;
            this.Systems.Remove(system);
        }
    }
}
