using System;

namespace ecsPractice
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (EcsPracticeGame game = new EcsPracticeGame())
            {
                game.Run();
            }
        }
    }
#endif
}

