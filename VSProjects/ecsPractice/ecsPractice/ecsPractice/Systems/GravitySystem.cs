﻿using ecsPractice.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Systems
{
    public class GravitySystem : EcsSystem
    {
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;

        public GravitySystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }

        // TODO: Mass is ignored by this system currently because I don't want to figure out the physics. But maybe I should. Later.
        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> wrappers = EcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);

            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                var movementComponent = wrapper.Components.OfType<MovableComponent>().Single();

                movementComponent.Velocity += Vector2.UnitY * 0.65f;
            }
        }

        protected override bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<MovableComponent>().Any() ||
                !entity.Components.OfType<MassiveComponent>().Any())
            { return false; }

            return true;
        }
    }
}
