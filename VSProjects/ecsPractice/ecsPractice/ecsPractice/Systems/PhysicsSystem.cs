﻿using ecsPractice.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Systems
{
    public class PhysicsSystem : EcsSystem
    {
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = 2;

        public PhysicsSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }

        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> wrappers = EcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);

            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                // TODO: Do all these references still mean that the changes get applied to the entity overall?
                var locationComponent = wrapper.Components.OfType<PhysicalComponent>().Single();
                var movementComponent = wrapper.Components.OfType<MovableComponent>().Single();

                locationComponent.Position += movementComponent.Velocity;
            }
        }

        protected override bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<MovableComponent>().Any())
                { return false; }

            return true;
        }
    }
}
