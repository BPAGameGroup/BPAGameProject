﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Systems
{
    // TODO: Movement system will apply MovementIntents to entity velocities (in MoveableComponent)?
    // This won't work for user interface; it doesn't have a velocity but still accepts movement intents?
    public class MovementIntentSystem : EcsSystem
    {
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;

        public MovementIntentSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        protected override bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            throw new NotImplementedException();
        }
    }
}
