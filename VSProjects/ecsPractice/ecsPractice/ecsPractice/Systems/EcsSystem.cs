﻿using ecsPractice.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Systems
{
    public abstract class EcsSystem
    {
        public enum SYSTEM_TYPE { Startup, Initialization, Background, Gameplay, UI, Draw };

        public SystemManager Parent;
        public SYSTEM_TYPE Type { get; set; }
        public int ExecutionOrder { get; set; }
        public IEnumerable<ILogger> Loggers { get; set; }

        public EcsSystem(SystemManager parent, SYSTEM_TYPE type, int executionOrder, params ILogger[] loggers)
        {
            this.Parent = parent;
            this.Type = type;
            this.ExecutionOrder = executionOrder;
            this.Loggers = loggers;
        }

        public abstract void Update();

        protected abstract bool HasRequiredComponents(EcsEntityWrapper entity);

        protected void Log(string text)
        {
            if (this.Loggers.Count() == 0)
                { throw new InvalidOperationException("This system has no loggers attached to it."); }

            DateTime time = DateTime.UtcNow;

            foreach (ILogger logger in this.Loggers)
            {
                logger.Log(text, time);
            }
        }

        // TODO: All of this required component, wrapper stuff could be simplified as just returning a list of collections of the components that system operates on, distinct as by the EntityID property.
        public static IEnumerable<EcsEntityWrapper> GetEntityWrappers(EntityManager manager)
        {
            List<EcsComponent> components = manager.Components;

            var ids = components.Select<EcsComponent, int>(comp => comp.EntityID).Distinct<int>();
            return components.GroupBy(comp => comp.EntityID,
                                      comp => comp, 
                                      (key, g) => new EcsEntityWrapper(key, g.ToArray()));
            // LINQ query equivalent of above line:
            /*var grouped = from comp in components
                          group comp by comp.EntityID into g
                          select new EcsEntityWrapper(g.Key, g.ToArray());*/
        }
    }
}
