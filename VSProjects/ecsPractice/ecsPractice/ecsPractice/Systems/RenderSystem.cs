﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ecsPractice.Components;

namespace ecsPractice.Systems
{
    public class RenderSystem : EcsSystem
    {
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Draw;
        private const int EXECUTION_ORDER = -1;

        private SpriteBatch _batch;

        public RenderSystem(SystemManager parent, SpriteBatch batch, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        {
            this._batch = batch;
        }

        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> wrappers = EcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);
            
            this._batch.Begin();
            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                this.Log(String.Format("DRAW_{0}", wrapper.ID));

                this._batch.Draw(wrapper.Components.OfType<RenderableComponent>().Single().Texture,
                                 wrapper.Components.OfType<PhysicalComponent>().Single().Position,
                                 Microsoft.Xna.Framework.Color.White);
            }
            // TODO: Will this _batch.End() execute if exceptions occur? Maybe wrap in try/catch/finally block.
            this._batch.End();
        }

        protected override bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<RenderableComponent>().Any() ||
                !entity.Components.OfType<PhysicalComponent>().Any())
                { return false; }

            return true;
        }
    }
}
