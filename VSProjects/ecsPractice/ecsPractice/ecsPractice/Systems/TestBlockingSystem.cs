﻿using ecsPractice.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Systems
{
    public class TestBlockingSystem : EcsSystem
    {
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;

        public TestBlockingSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }

        public override void Update()
        {
            List<CollisionEvent> collisions = this.Parent.CollisionEvents;

            foreach (CollisionEvent collision in collisions)
            {
                HandleCollision(collision);
            }
        }

        private void HandleCollision(CollisionEvent collision)
        {
            EcsComponent[] entityOne = this.Parent.Parent.EntityManager.GetEntity(collision.Entity1Id);
            EcsComponent[] entityTwo = this.Parent.Parent.EntityManager.GetEntity(collision.Entity2Id);

            EcsComponent[] movableEntity;
            if (entityOne.OfType<MovableComponent>().Any())
                { movableEntity = entityOne; }
            else if (entityTwo.OfType<MovableComponent>().Any())
                { movableEntity = entityTwo; }
            else
                { throw new ArgumentException("Neither of the given entities is movable.", nameof(collision)); }

            EcsComponent[] otherEntity;
            if (movableEntity[0].EntityID == collision.Entity1Id)
                { otherEntity = entityTwo; }
            else
                { otherEntity = entityOne; }

            //ZeroVelocity(movableEntity[0].EntityID);

            Vector2 movablePos = movableEntity.OfType<PhysicalComponent>().Single().Position;
            (int, int) movableBounds = movableEntity.OfType<CollideableComponent>().Single().BoundingBox;
            Vector2 movableCenter = FindCenter(movablePos, movableBounds);

            Vector2 otherPos = otherEntity.OfType<PhysicalComponent>().Single().Position;
            (int, int) otherBounds = movableEntity.OfType<CollideableComponent>().Single().BoundingBox;
            Vector2 otherCenter = FindCenter(otherPos, otherBounds);

            // TODO: Make velocity stop based on collision angle.
        }

        private Vector2 FindCenter(Vector2 pos, (int Width, int Height) bounds)
        {
            float x = pos.X + (bounds.Width / 2);
            float y = pos.Y + (bounds.Height / 2);

            return new Vector2(x, y);
        }

        private void ZeroVelocity(int entityId)
        {
            this.Parent.Parent.EntityManager.Components.Where(comp => comp.EntityID == entityId).OfType<MovableComponent>().Single().Velocity = Vector2.Zero;
        }

        private static bool IsColliding(Vector2 aPos, (int Width, int Height) aBounds,
                                        Vector2 bPos, (int Width, int Height) bBounds)
        {
            if (aBounds.Width < 0 || aBounds.Height < 0)
                { throw new ArgumentException("Bounds for given entity A are invalid.", nameof(aBounds)); }
            if (bBounds.Width < 0 || bBounds.Height < 0)
                { throw new ArgumentException("Bounds for given entity B are invalid.", nameof(bBounds)); }

            return IsColliding(aPos,
                               new Vector2(aPos.X + aBounds.Width, aPos.Y + aBounds.Height),
                               bPos,
                               new Vector2(bPos.X + bBounds.Width, bPos.Y + bBounds.Height));
        }

        private static bool IsColliding(Vector2 aTopLeft, Vector2 aBotRight, Vector2 bTopLeft, Vector2 bBotRight)
        {
            // Extract individual values from vectors
            float atlx = aTopLeft.X;
            float atly = aTopLeft.Y;

            float abrx = aBotRight.X;
            float abry = aBotRight.Y;

            float btlx = bTopLeft.X;
            float btly = bTopLeft.Y;

            float bbrx = bBotRight.X;
            float bbry = bBotRight.Y;

            // 8 positive comparisons
            //bool atlxToBtlx = atlx > btlx;
            //bool atlxToBbrx = atlx > bbrx;

            //bool atlyToBtly = atly > btly;
            //bool atlyToBbry = atly > bbry;

            //bool abrxToBtlx = abrx > btlx;
            //bool abrxToBbrx = abrx > bbrx;

            //bool abryToBtly = abry > btly;
            //bool abryToBbry = abry > bbry;
            bool atlxToBtlx = atlx >= btlx;
            bool atlxToBbrx = atlx >= bbrx;

            bool atlyToBtly = atly >= btly;
            bool atlyToBbry = atly >= bbry;

            bool abrxToBtlx = abrx >= btlx;
            bool abrxToBbrx = abrx >= bbrx;

            bool abryToBtly = abry >= btly;
            bool abryToBbry = abry >= bbry;

            // XORs each pair of comparisons to determine if each corner is between the corners of the other box on each axis.
            // Example: atlxInside determines if the top left corner of object A is between the corners of object B on the X axis.
            bool atlxInside = atlxToBtlx != atlxToBbrx;

            bool atlyInside = atlyToBtly != atlyToBbry;

            bool abrxInside = abrxToBtlx != abrxToBbrx;

            bool abryInside = abryToBtly != abryToBbry;

            // ANDs each pair of between comparisons to determine if each corner of object A is inside object A.
            // Example: If the top left corner of object A is inside object B on both the X axis and the Y axis, that corner of object A is inside object B.
            bool atlInside = atlxInside && atlyInside;

            bool abrInside = abrxInside && abryInside;

            // If either corner or both of object A is inside object B, then they are colliding.
            return atlInside || abrInside;
        }

        private bool HasRequiredComponents(EcsComponent[] entity)
        {
            if (!entity.OfType<PhysicalComponent>().Any() ||
                !entity.OfType<CollideableComponent>().Any() ||
                !entity.OfType<BlockingComponent>().Any())
                { return false; }

            return true;
        }

        private bool HasMovingComponents(EcsComponent[] entity)
        {
            if (!HasRequiredComponents(entity) ||
                !entity.OfType<MovableComponent>().Any())
                { return false; }

            return true;
        }

        protected override bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<CollideableComponent>().Any() ||
                !entity.Components.OfType<BlockingComponent>().Any())
            { return false; }

            return true;
        }

        private bool HasMovingComponents(EcsEntityWrapper entity)
        {
            if (!HasRequiredComponents(entity) ||
                !entity.Components.OfType<MovableComponent>().Any())
            { return false; }

            return true;
        }
    }
}
