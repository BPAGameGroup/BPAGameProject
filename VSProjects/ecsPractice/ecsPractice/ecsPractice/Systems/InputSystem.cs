﻿using ecsPractice.Components;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ecsPractice.Systems
{
    public class InputSystem : EcsSystem
    {
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Background;
        private const int EXECUTION_ORDER = 1;

        public InputSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }

        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> wrappers = EcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);
            KeyboardState state = Keyboard.GetState();

            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                var intents = wrapper.Components.OfType<MovementIntentsComponent>().Single();
                intents.ResetIntents();

                if (state.IsKeyDown(Keys.Left))
                {
                    intents.LeftIntent = true;
                    this.Log("INPUT_LeftArrowKey");
                }
                if (state.IsKeyDown(Keys.Right))
                {
                    intents.RightIntent = true;
                    this.Log("INPUT_RightArrowKey");
                }
                if (state.IsKeyDown(Keys.Space))
                {
                    intents.JumpIntent = true;
                    this.Log("INPUT_SpaceKey");
                }
            }
        }

        protected override bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<MovementIntentsComponent>().Any() ||
                !entity.Components.OfType<MovableComponent>().Any())
                { return false; }

            return true;
        }
    }
}
