using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ecsPractice.Systems;
using ecsPractice.Components;
using System.Text.RegularExpressions;

namespace ecsPractice
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class EcsPracticeGame : Microsoft.Xna.Framework.Game
    {
        public EntityManager EntityManager;
        public SystemManager SystemManager;

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Texture2D _playerTexture;
        private Texture2D _blockTexture;

        private FileLogger _fileLogger;

        public EcsPracticeGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this._spriteBatch = new SpriteBatch(GraphicsDevice);

            this._playerTexture = Content.Load<Texture2D>("player");
            this._blockTexture = Content.Load<Texture2D>("block");

            string time = DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss");
            time = Regex.Replace(time, @"[\s]", @"_");
            this._fileLogger = new FileLogger(String.Format(@"{0}\{1}.txt", Environment.CurrentDirectory, time));

            // TODO: use this.Content to load your game content here

            this.EntityManager = new EntityManager(this);
            this.SystemManager = new SystemManager(this);
            this.SystemManager.Add(new RenderSystem(this.SystemManager, this._spriteBatch, this._fileLogger));
            this.SystemManager.Add(new GravitySystem(this.SystemManager, this._fileLogger));
            this.SystemManager.Add(new PhysicsSystem(this.SystemManager, this._fileLogger));
            this.SystemManager.Add(new InputSystem(this.SystemManager, this._fileLogger));
            this.SystemManager.Add(new CollisionSystem(this.SystemManager, this._fileLogger));
            //this.SystemManager.Add(new BlockingSystem(this.SystemManager, this._fileLogger));
            this.SystemManager.Add(new TestBlockingSystem(this.SystemManager, this._fileLogger));

            var playerComponents = new EcsComponent[] { new PhysicalComponent(this.EntityManager, new Vector2(50, 50)),
                                                        new RenderableComponent(this.EntityManager, this._playerTexture),
                                                        new MovableComponent(this.EntityManager, Vector2.Zero),
                                                        new MassiveComponent(this.EntityManager, 1),
                                                        new MovementIntentsComponent(this.EntityManager),
                                                        new CollideableComponent(this.EntityManager, (this._playerTexture.Width, this._playerTexture.Height)),
                                                        new BlockingComponent(this.EntityManager), };
            this.EntityManager.AddEntity(playerComponents);

            var blockComponents = new EcsComponent[] { new PhysicalComponent(this.EntityManager, new Vector2(50, 200)),
                                                       new RenderableComponent(this.EntityManager, this._blockTexture),
                                                       new CollideableComponent(this.EntityManager, (this._blockTexture.Width, this._blockTexture.Height)),
                                                       new BlockingComponent(this.EntityManager), };
            this.EntityManager.AddEntity(blockComponents);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

            this._fileLogger.Dispose();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            this.SystemManager.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.White);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
