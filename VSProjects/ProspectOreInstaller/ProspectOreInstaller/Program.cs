﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using System.Reflection;

namespace ProspectOreInstaller
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program will output the game files to the install directory.");
            Console.WriteLine("Which directory to install to? Leave blank to install to the current directory.");
            string dir = Console.ReadLine();

            string dirPath;
            if (!String.IsNullOrEmpty(dir) && Uri.IsWellFormedUriString(dir, UriKind.RelativeOrAbsolute))
            {
                dirPath = dir;
            }
            else
            {
                dirPath = Environment.CurrentDirectory;
            }

            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream zipStream = assembly.GetManifestResourceStream("ProspectOreInstaller.archive.zip");

            FastZip fz = new FastZip();
            string fileFilter = null;
            
            fz.ExtractZip(zipStream,
                          dirPath,
                          FastZip.Overwrite.Always,
                          (str) => true,
                          fileFilter,
                          null,
                          false,
                          true);
        }
    }
}
