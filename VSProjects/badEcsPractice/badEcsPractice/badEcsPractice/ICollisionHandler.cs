﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace badEcsPractice
{
    interface ICollisionHandler
    {
        void Handle(Entity obj1, Entity obj2);
    }
}
