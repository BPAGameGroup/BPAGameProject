﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace badEcsPractice
{
    public class Entity
    {
        public int Id;
        public List<Component> Components;

        public Entity(int id)
        {
            this.Id = id;
            this.Components = new List<Component>();
        }
    }
}
