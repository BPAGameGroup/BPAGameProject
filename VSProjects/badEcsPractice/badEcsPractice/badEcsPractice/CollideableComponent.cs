﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace badEcsPractice
{
    public class CollideableComponent : Component
    {
        private readonly Entity _parent;

        public Rectangle BoundingBox
        {
            get { return this.GetBoundingBox(); }
        }

        public CollideableComponent(Entity entity)
        {
            this._parent = entity;
        }

        private Rectangle GetBoundingBox()
        {
            List<Component> comp = this._parent.Components;

            var pos = comp.OfType<PhysicalComponent>().Single();
            var tex = comp.OfType<RenderableComponent>().Single();

            Texture2D texture = tex.Texture;

            return new Rectangle((int)pos.X, (int)pos.Y, texture.Width, texture.Height);
        }
    }
}
