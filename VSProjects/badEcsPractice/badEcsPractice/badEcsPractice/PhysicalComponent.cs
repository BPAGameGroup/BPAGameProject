﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace badEcsPractice
{
    public class PhysicalComponent : Component
    {
        public int X;
        public int Y;
        
        public PhysicalComponent(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
