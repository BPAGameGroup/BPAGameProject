﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace badEcsPractice
{
    public class RenderableComponent : Component
    {
        public Texture2D Texture;

        public RenderableComponent(Texture2D texture)
        {
            this.Texture = texture;
        }
    }
}
