﻿using miningGame.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame
{
    /// <summary>
    /// Represents a wrapper for an ECS entity, containing its ID and components.
    /// </summary>
    [Obsolete("The ECSEntityWrapper is deprecated. Systems should directly interact with components.", false)]
    public class EcsEntityWrapper
    {
        #region Members definition
        public int ID
        {
            get { return Components[0].EntityID; }
        }
        public List<EcsComponent> Components { get; private set; }
        #endregion Members definition

        #region Constructors definition
        public EcsEntityWrapper(int id, params EcsComponent[] components)
        {
            this.Components = new List<EcsComponent>(components);
        }
        #endregion Constructors definition
    }
}
