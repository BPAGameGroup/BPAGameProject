﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame
{
    // TODO: This is a thin and arguably pointless wrapper.
    /// <summary>
    /// Represents a game action intent.
    /// </summary>
    public class Intent
    {
        #region Members definition
        public string Type;
        #endregion Members definition

        #region Constructors definition
        public Intent(string type)
        {
            this.Type = type;
        }
        #endregion Constructors definition
    }
}
