﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    public class MusicSystem : UpdateableEcsSystem
    {
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;

        private readonly List<Song> _songs;
        private int _currentSongIndex;


        public MusicSystem(SystemManager manager, params ILogger[] loggers)
            : base(manager, TYPE, EXECUTION_ORDER, loggers)
        {
            this._songs = new List<Song>();
        }

        private void Start()
        {
            throw new NotImplementedException();
        }

        public override void Update()
        {
            throw new NotImplementedException();

            Song test = this._songs[0];
            TimeSpan span = test.Duration;
            DateTime before = DateTime.Now.AddMinutes(-3);
        }

        public void AddSong(Song song)
        {
            if (song == null)
                { throw new ArgumentNullException("The given Song is null.", nameof(song)); }

            this._songs.Add(song);
        }
    }
}
