﻿using miningGame.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that detects collisions and notifies other systems when they occur.
    /// </summary>
    public class CollisionSystem : UpdateableEcsSystem
    {
        #region Members definition
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;
        #endregion Members definition

        #region Constructors definition
        public CollisionSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Determines if the two given rectangles are colliding.
        /// </summary>
        /// <param name="aTopLeft">The top left corner of rectangle A.</param>
        /// <param name="aBotRight">The bottom right corner of rectangle A.</param>
        /// <param name="bTopLeft">The top left corner of rectangle B.</param>
        /// <param name="bBotRight">The bottom right corner of rectangle B.</param>
        /// <returns>Boolean true if they are colliding, boolean false if not.</returns>
        private bool IsColliding(Vector2 aTopLeft, Vector2 aBotRight, Vector2 bTopLeft, Vector2 bBotRight)
        {
            // Extract individual values from vectors
            float atlx = aTopLeft.X;
            float atly = aTopLeft.Y;

            float abrx = aBotRight.X;
            float abry = aBotRight.Y;

            float btlx = bTopLeft.X;
            float btly = bTopLeft.Y;

            float bbrx = bBotRight.X;
            float bbry = bBotRight.Y;

            // 8 positive comparisons
            bool atlxToBtlx = atlx > btlx;
            bool atlxToBbrx = atlx > bbrx;

            bool atlyToBtly = atly > btly;
            bool atlyToBbry = atly > bbry;

            bool abrxToBtlx = abrx > btlx;
            bool abrxToBbrx = abrx > bbrx;

            bool abryToBtly = abry > btly;
            bool abryToBbry = abry > bbry;

            // XORs each pair of comparisons to determine if each corner is between the corners of the other box on each axis.
            // Example: atlxInside determines if the top left corner of object A is between the corners of object B on the X axis.
            bool atlxInside = atlxToBtlx != atlxToBbrx;

            bool atlyInside = atlyToBtly != atlyToBbry;

            bool abrxInside = abrxToBtlx != abrxToBbrx;

            bool abryInside = abryToBtly != abryToBbry;

            // ANDs each pair of between comparisons to determine if each corner of object A is inside object A.
            // Example: If the top left corner of object A is inside object B on both the X axis and the Y axis, that corner of object A is inside object B.
            bool atlInside = atlxInside && atlyInside;

            bool abrInside = abrxInside && abryInside;

            // If either corner or both of object A is inside object B, then they are colliding.
            return atlInside || abrInside;
        }

        /// <summary>
        /// Determines if the given entity (represented by a wrapper) has the required components for this system to operate on it.
        /// </summary>
        /// <param name="entity">The entity to check.</param>
        /// <returns>Boolean true if the given entity contains the required components, boolean false if not.</returns>
        protected bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<CollideableComponent>().Any())
            { return false; }

            return true;
        }

        #region Overrides definition
        /// <summary>
        /// Checks and registers collisions between all game entities.
        /// </summary>
        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> entities = EcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);

            foreach (EcsEntityWrapper entity1 in entities)
            {
                if (!this.HasRequiredComponents(entity1))
                    { continue; }

                foreach (EcsEntityWrapper entity2 in entities)
                {
                    if (entity2.ID == entity1.ID)
                        { continue; }
                    
                    if (!this.HasRequiredComponents(entity2))
                        { continue; }

                    Vector2 e1Pos = entity1.Components.OfType<PhysicalComponent>().Single().Position;
                    (int Width, int Height) e1Bounds = entity1.Components.OfType<CollideableComponent>().Single().BoundingBox;

                    Vector2 e2Pos = entity2.Components.OfType<PhysicalComponent>().Single().Position;
                    (int Width, int Height) e2Bounds = entity2.Components.OfType<CollideableComponent>().Single().BoundingBox;

                    if (this.IsColliding(new Vector2(e1Pos.X, e1Pos.Y),
                                         new Vector2(e1Pos.X + e1Bounds.Width, e1Pos.Y + e1Bounds.Height),
                                         new Vector2(e2Pos.X, e2Pos.Y),
                                         new Vector2(e2Pos.X + e2Bounds.Width, e2Pos.Y + e2Bounds.Height)))
                    {
                        this.Parent.CollisionEvents.Add(new CollisionEvent(entity1.ID, entity2.ID));
                        this.Log(String.Format("COLLISION_{0}-{1}", entity1.ID, entity2.ID));
                    }
                }
            }
        }
        #endregion Overrides definition
        #endregion Methods definition
    }
}
