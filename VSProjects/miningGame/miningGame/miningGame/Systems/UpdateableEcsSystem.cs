﻿using miningGame.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents a base System in an Entity-Component-System architecture.
    /// Defines an interface that allows consumers to invoke the system to take its action.
    /// </summary>
    public abstract class UpdateableEcsSystem : EcsSystem
    {
        #region Constructors definition
        public UpdateableEcsSystem(SystemManager parent, SYSTEM_TYPE type, int executionOrder, params ILogger[] loggers)
            : base(parent, type, executionOrder, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        #region Abstract methods declaration
        public abstract void Update();
        #endregion Abstract methods declaration
        #endregion Methods definition
    }
}
