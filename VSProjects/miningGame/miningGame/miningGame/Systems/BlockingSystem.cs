﻿using miningGame.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that prevents solid objects from intersecting with each other.
    /// </summary>
    [Obsolete("BlockingSystem is deprecated. Use IntersectionSystem instead.", false)]
    public class BlockingSystem : UpdateableEcsSystem
    {
        #region Members definition
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = 2;
        #endregion Members definition

        #region Constructors definition
        public BlockingSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        private static bool IsColliding(Vector2 aTopLeft, Vector2 aBotRight, Vector2 bTopLeft, Vector2 bBotRight)
        {
            // Extract individual values from vectors
            float atlx = aTopLeft.X;
            float atly = aTopLeft.Y;

            float abrx = aBotRight.X;
            float abry = aBotRight.Y;

            float btlx = bTopLeft.X;
            float btly = bTopLeft.Y;

            float bbrx = bBotRight.X;
            float bbry = bBotRight.Y;

            // 8 positive comparisons
            //bool atlxToBtlx = atlx > btlx;
            //bool atlxToBbrx = atlx > bbrx;

            //bool atlyToBtly = atly > btly;
            //bool atlyToBbry = atly > bbry;

            //bool abrxToBtlx = abrx > btlx;
            //bool abrxToBbrx = abrx > bbrx;

            //bool abryToBtly = abry > btly;
            //bool abryToBbry = abry > bbry;
            bool atlxToBtlx = atlx >= btlx;
            bool atlxToBbrx = atlx >= bbrx;

            bool atlyToBtly = atly >= btly;
            bool atlyToBbry = atly >= bbry;

            bool abrxToBtlx = abrx >= btlx;
            bool abrxToBbrx = abrx >= bbrx;

            bool abryToBtly = abry >= btly;
            bool abryToBbry = abry >= bbry;

            // XORs each pair of comparisons to determine if each corner is between the corners of the other box on each axis.
            // Example: atlxInside determines if the top left corner of object A is between the corners of object B on the X axis.
            bool atlxInside = atlxToBtlx != atlxToBbrx;

            bool atlyInside = atlyToBtly != atlyToBbry;

            bool abrxInside = abrxToBtlx != abrxToBbrx;

            bool abryInside = abryToBtly != abryToBbry;

            // ANDs each pair of between comparisons to determine if each corner of object A is inside object A.
            // Example: If the top left corner of object A is inside object B on both the X axis and the Y axis, that corner of object A is inside object B.
            bool atlInside = atlxInside && atlyInside;

            bool abrInside = abrxInside && abryInside;

            // If either corner or both of object A is inside object B, then they are colliding.
            return atlInside || abrInside;
        }

        private static Vector2 FindValidPosition(Vector2 movingPos, Vector2 movingDest, (int Width, int Height) movingBounds,
                                                 Vector2 blockingPos, (int Width, int Height) blockingBounds)
        {
            MovementWrapper wrapper = new MovementWrapper(movingPos, movingDest, movingBounds);

            for (int i = 0; i < wrapper.MovementStepCount; i++)
            {
                Vector2 positionAttempt = movingPos + (wrapper.MovementStep * i);

                if (!IsColliding(positionAttempt,
                                 new Vector2(positionAttempt.X + movingBounds.Width, positionAttempt.Y + movingBounds.Height),
                                 blockingPos,
                                 new Vector2(blockingPos.X + blockingBounds.Width, blockingPos.Y + blockingBounds.Height)))
                {
                    wrapper.Position = positionAttempt;
                }
                else
                {
                    if (wrapper.IsDiagonal)
                    {
                        int stepsLeft = wrapper.MovementStepCount - i - 1;

                        Vector2 horizontalMovementRemaining = wrapper.MovementStep.X * Vector2.UnitX * stepsLeft;
                        wrapper.Position = FindValidPosition(wrapper.Position,
                                                             wrapper.Position + horizontalMovementRemaining,
                                                             movingBounds,
                                                             blockingPos,
                                                             blockingBounds);

                        Vector2 verticalMovementRemaining = wrapper.MovementStep.Y * Vector2.UnitY * stepsLeft;
                        wrapper.Position = FindValidPosition(wrapper.Position,
                                                             wrapper.Position + verticalMovementRemaining,
                                                             movingBounds,
                                                             blockingPos,
                                                             blockingBounds);
                    }

                    break;
                }
            }

            return wrapper.Position;
        }

        private bool HasMovingComponents(EcsEntityWrapper entity)
        {
            if (!HasRequiredComponents(entity) ||
                !entity.Components.OfType<MovableComponent>().Any())
            { return false; }

            return true;
        }

        protected bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<CollideableComponent>().Any() ||
                !entity.Components.OfType<SolidComponent>().Any())
            { return false; }

            return true;
        }

        #region Overrides definition
        public override void Update()
        {
            var entities = UpdateableEcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);

            var moving = entities.Where(entity => this.HasMovingComponents(entity));
            var blocking = entities.Where(entity => this.HasRequiredComponents(entity) && !this.HasMovingComponents(entity));

            foreach (EcsEntityWrapper movingEntity in moving)
            {
                Vector2 movingPos = movingEntity.Components.OfType<PhysicalComponent>().Single().Position;
                Vector2 oldPos = movingPos;
                Vector2 movingVelocity = movingEntity.Components.OfType<MovableComponent>().Single().Velocity;
                (int Width, int Height) movingBounds = movingEntity.Components.OfType<CollideableComponent>().Single().BoundingBox;

                foreach (EcsEntityWrapper blockingEntity in blocking)
                {
                    Vector2 blockingPos = blockingEntity.Components.OfType<PhysicalComponent>().Single().Position;
                    (int Width, int Height) blockingBounds = blockingEntity.Components.OfType<CollideableComponent>().Single().BoundingBox;

                    Vector2 newPos = movingPos + movingVelocity;
                    if (IsColliding(newPos,
                                    new Vector2(newPos.X + movingBounds.Width, newPos.Y + movingBounds.Height),
                                    blockingPos,
                                    new Vector2(blockingPos.X + blockingBounds.Width, blockingPos.Y + blockingBounds.Height)))
                    {
                        // TODO: Player entity velocity doesn't actually get updated properly.
                        movingPos = FindValidPosition(movingPos, newPos, movingBounds,
                                                      blockingPos, blockingBounds);
                        //Console.WriteLine("COLLISION!");

                        Vector2 change = movingPos - oldPos;
                        if (change.X == 0) { movingVelocity *= Vector2.UnitY; }
                        if (change.Y == 0) { movingVelocity *= Vector2.UnitX; }

                        movingEntity.Components.OfType<PhysicalComponent>().Single().Position = movingPos;
                        movingEntity.Components.OfType<MovableComponent>().Single().Velocity = movingVelocity;
                    }
                    else
                    {
                        movingEntity.Components.OfType<PhysicalComponent>().Single().Position = newPos;
                    }
                }
            }
        }
        #endregion Overrides definition
        #endregion Methods definition
    }

    public struct MovementWrapper
    {
        public Vector2 Movement { get; private set; }
        public Vector2 Position { get; set; }
        public int MovementStepCount { get; private set; }
        public bool IsDiagonal { get; private set; }
        public Vector2 MovementStep { get; private set; }
        public (int Width, int Height) Bounds { get; set; }

        public MovementWrapper(Vector2 original, Vector2 destination, (int Width, int Height) bounds)
            : this()
        {
            this.Movement = destination - original;
            this.Position = original;
            this.MovementStepCount = (int)(this.Movement.Length() * 2) + 1;
            this.IsDiagonal = this.Movement.X != 0 && this.Movement.Y != 0;
            this.MovementStep = this.Movement / this.MovementStepCount;
            this.Bounds = bounds;
        }
    }
}
