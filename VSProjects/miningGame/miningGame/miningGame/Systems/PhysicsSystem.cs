﻿using miningGame.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that applies velocities to entity positions using vector transformations.
    /// </summary>
    public class PhysicsSystem : UpdateableEcsSystem
    {
        #region Members definition
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = 2;
        #endregion Members definition

        #region Constructors definition
        public PhysicsSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        #region Overrides definition
        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> wrappers = UpdateableEcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);

            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                // TODO: Do all these references still mean that the changes get applied to the entity overall?
                var locationComponent = wrapper.Components.OfType<PhysicalComponent>().Single();
                var movementComponent = wrapper.Components.OfType<MovableComponent>().Single();

                locationComponent.Position += movementComponent.Velocity;
            }
        }
        #endregion Overrides definition

        protected bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<MovableComponent>().Any())
            { return false; }

            return true;
        }
        #endregion Methods definition
    }
}
