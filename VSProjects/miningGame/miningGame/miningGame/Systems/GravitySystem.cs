﻿using miningGame.Components;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that handles applying gravitational acceleration to entities with mass.
    /// </summary>
    public class GravitySystem : UpdateableEcsSystem
    {
        #region Members definition
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;
        #endregion Members definition

        #region Constructors definition
        public GravitySystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        protected bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<PhysicalComponent>().Any() ||
                !entity.Components.OfType<MovableComponent>().Any() ||
                !entity.Components.OfType<MassiveComponent>().Any())
            { return false; }

            return true;
        }

        #region Overrides definition
        // TODO: Mass is ignored by this system currently because I don't want to figure out the physics. But maybe I should. Later.
        public override void Update()
        {
            IEnumerable<EcsEntityWrapper> wrappers = UpdateableEcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);

            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                var movementComponent = wrapper.Components.OfType<MovableComponent>().Single();

                movementComponent.Velocity += Vector2.UnitY * 0.65f;
            }
        }
        #endregion Overrides definition
        #endregion Methods definition
    }
}
