﻿using Microsoft.Xna.Framework;
using miningGame.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    public class PrototypeSystem : EcsSystem
    {
        #region Members definition
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Startup;
        private const int EXECUTION_ORDER = -1;

        private readonly string _jsonUri;
        #endregion Members definition

        #region Constructors definition
        public PrototypeSystem(string prototypesJsonPath, SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        {
            if (!Uri.IsWellFormedUriString(prototypesJsonPath, UriKind.RelativeOrAbsolute))
            {
                throw new ArgumentException("The given JSON file path is not well formed.", nameof(prototypesJsonPath));
            }

            this._jsonUri = prototypesJsonPath;
        }
        #endregion Constructors definition

        #region Methods definition
        public void BuildPrototypes()
        {
            var prototypes = new Dictionary<string, List<EcsComponent>>();

            JObject json = ReadJsonFile(this._jsonUri);
            
            List<JToken> entities = json.Value<JToken>("Prototypes").Children().ToList();
            
            foreach (JToken entity in entities)
            {
                var components = new List<EcsComponent>();
                EntityPrototype prototype = entity.ToObject<EntityPrototype>();

                foreach (ComponentPrototype component in prototype.Components)
                {
                    components.Add(this.GenerateEcsComponent(component));
                }

                prototypes.Add(prototype.Key, components);
            }

            this.Parent.EntityPrototypes = prototypes;
        }

        private EcsComponent GenerateEcsComponent(ComponentPrototype prototype)
        {
            EntityManager entityManager = this.Parent.Parent.EntityManager;
            // TODO: This is bad.
            switch (prototype.Type)
            {
                case "physical":
                    return new PhysicalComponent(entityManager, new Vector2());
                case "renderable":
                    return new RenderableComponent(entityManager, null);
                case "movable":
                    return new MovableComponent(entityManager, new Vector2());
                case "massive":
                    return new MassiveComponent(entityManager, -1);
                case "movementIntents":
                    return new MovementIntentsComponent(entityManager);
                case "collideable":
                    return new CollideableComponent(entityManager, (-1, -1));
                case "solid":
                    return new SolidComponent(entityManager);
                default:
                    throw new ArgumentException("The given component prototype did not have a valid Type property.", nameof(prototype));
            }
        }

        private static JObject ReadJsonFile(string path)
        {
            // TODO: Make this parse to an actual .NET object.
            using (StreamReader reader = new StreamReader(path))
            {
                var jsonReader = new JsonTextReader(reader);
                return JObject.Load(jsonReader);
            }
        }
        #endregion Methods definition
    }

    public struct EntityPrototype
    {
        public string Key;
        public List<ComponentPrototype> Components;
    }

    public struct ComponentPrototype
    {
        public string Type;
    }
}
