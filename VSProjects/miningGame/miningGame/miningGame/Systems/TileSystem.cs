﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    public class TileSystem : EcsSystem
    {
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Initialization;
        private const int EXECUTION_ORDER = -1;

        public TileSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
    }
}
