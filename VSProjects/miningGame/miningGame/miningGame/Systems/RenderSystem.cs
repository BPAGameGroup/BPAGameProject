﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using miningGame.Components;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that renders each visible entity to the screen.
    /// </summary>
    public class RenderSystem : UpdateableEcsSystem
    {
        #region Members definition
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Draw;
        private const int EXECUTION_ORDER = -1;

        private SpriteBatch _batch;
        #endregion Members definition

        #region Constructors definition
        public RenderSystem(SystemManager parent, SpriteBatch batch, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        {
            this._batch = batch;
        }
        #endregion Constructors definition

        #region Methods definition
        #region Overrides definition
        public override void Update()
        {
            int drawn = 0;
            IEnumerable<EcsEntityWrapper> wrappers = EcsSystem.GetEntityWrappers(this.Parent.Parent.EntityManager);
            
            this._batch.Begin();
            foreach (EcsEntityWrapper wrapper in wrappers)
            {
                if (!this.HasRequiredComponents(wrapper))
                    { continue; }

                drawn++;

                this._batch.Draw(wrapper.Components.OfType<RenderableComponent>().Single().Texture,
                                 wrapper.Components.OfType<PhysicalComponent>().Single().Position,
                                 Microsoft.Xna.Framework.Color.White);
            }
            this.Log(String.Format("DRAW - DREW {0} ENTITIES", drawn));
            // TODO: Will this _batch.End() execute if exceptions occur? Maybe wrap in try/catch/finally block.
            this._batch.End();
        }
        #endregion Overrides definition

        protected bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<RenderableComponent>().Any() ||
                !entity.Components.OfType<PhysicalComponent>().Any())
            { return false; }

            return true;
        }
        #endregion Methods definition
    }
}
