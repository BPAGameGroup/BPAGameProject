﻿using miningGame.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the base class for all ECS systems.
    /// </summary>
    public abstract class EcsSystem
    {
        #region Members definition
        #region Static members definition
        public enum SYSTEM_TYPE { Startup, Initialization, Background, Gameplay, UI, Draw };
        #endregion Static members definition

        #region Properties definition
        public SYSTEM_TYPE Type { get; set; }
        public int ExecutionOrder { get; set; }
        public IEnumerable<ILogger> Loggers { get; set; }
        #endregion Properties definition

        #region Fields definition
        public SystemManager Parent;
        #endregion Fields definition
        #endregion Members definition

        #region Constructors definition
        public EcsSystem(SystemManager parent, SYSTEM_TYPE type, int executionOrder, params ILogger[] loggers)
        {
            this.Parent = parent;
            this.Type = type;
            this.ExecutionOrder = executionOrder;
            this.Loggers = loggers;
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Logs the given message via all of this system's attached loggers.
        /// </summary>
        /// <param name="text">The text to log.</param>
        /// <exception cref="InvalidOperationException">Thrown if this system has no loggers attached to it.</exception>
        protected void Log(string text)
        {
            if (this.Loggers.Count() == 0)
            { throw new InvalidOperationException("This system has no loggers attached to it."); }

            DateTime time = DateTime.UtcNow;

            foreach (ILogger logger in this.Loggers)
            {
                logger.Log(text, time);
            }
        }

        // TODO: All of this required component, wrapper stuff could be simplified as just returning a list of collections of the components that system operates on, distinct as by the EntityID property.
        /// <summary>
        /// Gets entity wrappers for all entities in the given EntityManager.
        /// </summary>
        /// <param name="manager">The EntityManager to get entities from.</param>
        /// <returns>IEnumerable of entity wrappers.</returns>
        public static IEnumerable<EcsEntityWrapper> GetEntityWrappers(EntityManager manager)
        {
            List<EcsComponent> components = manager.Components;

            var ids = components.Select<EcsComponent, int>(comp => comp.EntityID).Distinct<int>();
            return components.GroupBy(comp => comp.EntityID,
                                      comp => comp,
                                      (key, g) => new EcsEntityWrapper(key, g.ToArray()));
            // LINQ query equivalent of above line:
            /*var grouped = from comp in components
                          group comp by comp.EntityID into g
                          select new EcsEntityWrapper(g.Key, g.ToArray());*/
        }
        #endregion Methods definition
    }
}
