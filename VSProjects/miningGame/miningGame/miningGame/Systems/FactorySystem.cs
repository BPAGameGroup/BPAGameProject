﻿using miningGame.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that constructs entities from known prototypes.
    /// </summary>
    public class FactorySystem : UpdateableEcsSystem
    {
        #region Members definition
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Initialization;
        private const int EXECUTION_ORDER = -1;
        #endregion Members definition

        #region Constructors definition
        public FactorySystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        #region Overrides definition
        /// <summary>
        /// Creates all currently queued entities.
        /// </summary>
        public override void Update()
        {
            List<string> entityKeys = this.Parent.EntitiesToCreate;

            foreach (string entityKey in entityKeys)
            {
                this.CreateEntity(entityKey);
            }
        }
        #endregion Overrides definition

        /// <summary>
        /// Creates entity based on given prototype key.
        /// </summary>
        /// <param name="typeKey">The key of the prototype to base the new entity from.</param>
        public void CreateEntity(string typeKey)
        {
            if (String.IsNullOrEmpty(typeKey))
                { throw new ArgumentException("The given key is invalid.", nameof(typeKey)); }

            List<EcsComponent> components = this.Parent.EntityPrototypes[typeKey];

            this.Parent.Parent.EntityManager.AddEntity(components.ToArray());
        }
        #endregion Methods definition
    }
}
