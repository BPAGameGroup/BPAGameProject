﻿using Microsoft.Xna.Framework;
using miningGame.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    // TODO: Movement system will apply MovementIntents to entity velocities (in MoveableComponent)?
    // This won't work for user interface; it doesn't have a velocity but still accepts movement intents?
    /// <summary>
    /// Represents the system that converts movement-related Intents to game state changes by applying vector transformations.
    /// </summary>
    public class MovementIntentSystem : UpdateableEcsSystem
    {
        #region Members definition
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Gameplay;
        private const int EXECUTION_ORDER = -1;
        #endregion Members definition

        #region Constructors definition
        public MovementIntentSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        #region Overrides definition
        public override void Update()
        {
            List<Intent> intents = this.Parent.Intents;
            IEnumerable<IGrouping<int, EcsComponent>> entities = this.Parent.Parent.EntityManager.Components.GroupBy(comp => comp.EntityID);
            foreach (IGrouping<int, EcsComponent> entity in entities)
            {
                List<EcsComponent> components = entity.ToList();

                if (!HasRequiredComponents(components))
                    { continue; }

                foreach (Intent intent in intents)
                {
                    this.ApplyIntent(components, intent);
                }
            }
        }
        #endregion Overrides definition

        private void ApplyIntent(List<EcsComponent> entity, Intent intent)
        {
            // TODO: This doesn't yet support much and it's a stupid method.
            MovableComponent component = entity.OfType<MovableComponent>().Single();

            switch (intent.Type)
            {
                case "MoveLeft":
                    component.Velocity += -Vector2.UnitX;
                    break;
                case "MoveRight":
                    component.Velocity += Vector2.UnitX;
                    break;
                case "Jump":
                    component.Velocity += -Vector2.UnitY * 20;
                    break;
            }
        }

        private bool HasRequiredComponents(List<EcsComponent> entity)
        {
            if (!entity.OfType<MovableComponent>().Any() ||
                !entity.OfType<MovementIntentsComponent>().Any())
            { return false; }

            return true;
        }
        #endregion Methods definition
    }
}
