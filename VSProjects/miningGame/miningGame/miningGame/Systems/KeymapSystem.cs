﻿using miningGame.Components;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Systems
{
    /// <summary>
    /// Represents the system that handles mapping from keypresses to ingame actions.
    /// </summary>
    public class KeymapSystem : UpdateableEcsSystem
    {
        #region Members definition
        // TODO: Determine execution order
        private const SYSTEM_TYPE TYPE = SYSTEM_TYPE.Background;
        private const int EXECUTION_ORDER = 1;
        #endregion Members definition

        #region Constructors definition
        public KeymapSystem(SystemManager parent, params ILogger[] loggers)
            : base(parent, TYPE, EXECUTION_ORDER, loggers)
        { }
        #endregion Constructors definition

        #region Methods definition
        #region Static members definition
        private Intent GetIntent(Keys key)
        {
            // TODO: Make these do stuff.
            switch (key)
            {
                case Keys.Up:
                case Keys.Space:
                    return new Intent("Jump");
                case Keys.Left:
                    return new Intent("MoveLeft");
                case Keys.Right:
                    return new Intent("MoveRight");
                default:
                    return null;
            }
        }
        #endregion Static members definition

        #region Overrides definition
        public override void Update()
        {
            // TODO: This system could support one key being mapped to multiple things without much more work?
            KeyboardState state = Keyboard.GetState();
            Keys[] pressed = state.GetPressedKeys();
            List<string> setIntents = new List<string>();

            foreach (Keys key in pressed)
            {
                Intent keyResult = GetIntent(key);

                if (keyResult == null)
                {
                    // No mapping for this key. Do nothing.
                    continue;
                }

                if (setIntents.Contains(keyResult.Type))
                {
                    // Mapped intent has already been activated. Do nothing.
                    continue;
                }

                setIntents.Add(keyResult.Type);
                this.Parent.Intents.Add(keyResult);
            }
        }
        #endregion Overrides definition

        protected bool HasRequiredComponents(EcsEntityWrapper entity)
        {
            if (!entity.Components.OfType<MovementIntentsComponent>().Any() ||
                !entity.Components.OfType<MovableComponent>().Any())
            { return false; }

            return true;
        }
        #endregion Methods definition
    }
}
