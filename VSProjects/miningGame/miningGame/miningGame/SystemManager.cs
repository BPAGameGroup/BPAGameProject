﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using miningGame.Systems;
using Microsoft.Xna.Framework.Graphics;
using miningGame.Components;

namespace miningGame
{
    // TODO: Decouple SystemManager from MiningGame using ISystemManager interface?
    /// <summary>
    /// Represents a manager for ECS systems.
    /// </summary>
    public class SystemManager
    {
        #region Members definition
        #region Static members definition
        private static readonly Dictionary<EcsSystem.SYSTEM_TYPE, int> SYSTEM_EXECUTION_ORDER = new Dictionary<EcsSystem.SYSTEM_TYPE, int>()
        {
            { EcsSystem.SYSTEM_TYPE.Background, 0 },
            { EcsSystem.SYSTEM_TYPE.Gameplay, 1 },
            { EcsSystem.SYSTEM_TYPE.UI, 1 },
            { EcsSystem.SYSTEM_TYPE.Draw, 2 },
        };
        #endregion Static members definition

        #region Properties definition
        public MiningGame Parent { get; set; }
        #endregion Properties definition

        #region Fields definition
        public List<EcsSystem> Systems = new List<EcsSystem>();
        public List<CollisionEvent> CollisionEvents = new List<CollisionEvent>();
        public List<Intent> Intents = new List<Intent>();
        public List<string> EntitiesToCreate = new List<string>();
        public Dictionary<string, List<EcsComponent>> EntityPrototypes;
        #endregion Fields definition
        #endregion Members definition

        #region Constructors definition
        public SystemManager(MiningGame parent)
        {
            this.Parent = parent;
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Runs all initialization systems in this manager's set.
        /// </summary>
        public void Initialize()
        {
            PrototypeSystem prototypeSystem = this.Systems.Where(system => system is PrototypeSystem).Single() as PrototypeSystem;
            prototypeSystem.BuildPrototypes();
            // TODO: Make this use the prototype system to create entities.
        }

        /// <summary>
        /// Runs all updateable systems in this manager's set.
        /// </summary>
        public void Update()
        {
            IEnumerable<UpdateableEcsSystem> updateableSystems = this.Systems.Where(system => system is UpdateableEcsSystem).Cast<UpdateableEcsSystem>();
            IEnumerable<IGrouping<EcsSystem.SYSTEM_TYPE, UpdateableEcsSystem>> grouped = updateableSystems.GroupBy(sys => sys.Type, sys => sys);
            IOrderedEnumerable<IGrouping<EcsSystem.SYSTEM_TYPE, UpdateableEcsSystem>> ordered = grouped.OrderBy(sys => SYSTEM_EXECUTION_ORDER[sys.Key]);

            foreach (IGrouping<EcsSystem.SYSTEM_TYPE, UpdateableEcsSystem> type in ordered)
            {
                List<UpdateableEcsSystem> systems = type.ToList();
                List<UpdateableEcsSystem> orderedSystems = systems.OrderBy(sys => sys.ExecutionOrder).ToList();

                foreach (UpdateableEcsSystem system in orderedSystems)
                {
                    system.Update();
                }
            }

            this.ClearFrameEvents();
        }

        /// <summary>
        /// Clears all of this manager's event queues.
        /// </summary>
        private void ClearFrameEvents()
        {
            this.CollisionEvents.Clear();
            this.EntitiesToCreate.Clear();
            this.Intents.Clear();
        }

        /// <summary>
        /// Queues an entity prototype for creation by relevant systems.
        /// </summary>
        /// <param name="typeKey">The prototype key to create.</param>
        /// <exception cref="ArgumentException">Thrown if the given key does not map to an existing prototype.</exception>
        public void QueueEntity(string typeKey)
        {
            if (String.IsNullOrEmpty(typeKey))
                { throw new ArgumentException("The given key is invalid.", nameof(typeKey)); }

            this.EntitiesToCreate.Add(typeKey);
        }

        /// <summary>
        /// Adds the given system to this manager's set.
        /// </summary>
        /// <param name="system">The system to add.</param>
        public void Add(EcsSystem system)
        {
            system.Parent = this;
            this.Systems.Add(system);
        }

        /// <summary>
        /// Removes the given system from this manager's set.
        /// </summary>
        /// <param name="system">The system to remove.</param>
        public void Remove(EcsSystem system)
        {
            system.Parent = null;
            this.Systems.Remove(system);
        }
        #endregion Methods definition
    }
}
