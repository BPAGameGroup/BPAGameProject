using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text.RegularExpressions;
using miningGame.Systems;
using miningGame.Components;
using System.IO;
using System.Threading;

namespace miningGame
{
    /// <summary>
    /// Represents the entire game object.
    /// </summary>
    public class MiningGame : Microsoft.Xna.Framework.Game
    {
        #region Members definition
        #region Constant members definition
        private const int WINDOW_WIDTH = 960;
        private const int WINDOW_HEIGHT = 640;

        private const string PROTOTYPES_FILE_PATH = "./entityPrototypes.json";
        #endregion Constant members definition

        #region Static members definition
        private static readonly Vector2 _errorTextPosition = new Vector2((float)(WINDOW_HEIGHT * 0.1),
                                                                         (float)(WINDOW_WIDTH * 0.5));
        #endregion Static members definition

        public EntityManager EntityManager;
        public SystemManager SystemManager;

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Texture2D _playerTexture;
        private Texture2D _caveBkgTexture;
        private Texture2D _grassFloorTexture;

        private List<Song> _songCollection;

        private SpriteFont _uiFont;

        private List<ILogger> _loggers = new List<ILogger>();
        #endregion Members definition

        #region Constructors definition
        public MiningGame()
        {
            this._graphics = new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";

            this._graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            this._graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Loads all assets and defines values for corresponding properties.
        /// TODO: This should be done by initialization systems, not the game class.
        /// </summary>
        private void LoadAssets()
        {
            //Content.Load<Texture2D>("AGAGAGE");
            this._playerTexture = Content.Load<Texture2D>("MineMan1");
            this._caveBkgTexture = Content.Load<Texture2D>("CaveBkg");
            this._grassFloorTexture = Content.Load<Texture2D>("GrassTile");

            List<Song> songs = new List<Song>();
            songs.Add(Content.Load<Song>("dapperDanClean"));
            songs.Add(Content.Load<Song>("defendAmericaClean"));
            songs.Add(Content.Load<Song>("doggoneBlueClean"));
            this._songCollection = songs;
        }

        /// <summary>
        /// Initializes all event loggers (ILoggers) and adds to collection.
        /// </summary>
        private void InitializeLoggers()
        {
            string time = DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss");
            time = Regex.Replace(time, @"[\s]", @"_");
            // TODO: May be best to always add a dummy logger?
            try
            {
                this._loggers.Add(new FileLogger(String.Format(@"{0}\{1}.txt", Environment.CurrentDirectory, time)));
            }
            catch (Exception ex) when (ex is UnauthorizedAccessException ||
                                       ex is FileNotFoundException ||
                                       ex is IOException ||
                                       ex is DirectoryNotFoundException ||
                                       ex is PathTooLongException)
            {
                // File logger can't get access to the file. Use a dummy logger to prevent exceptions down the line.
                this._loggers.Add(new DummyLogger());
            }
        }

        /// <summary>
        /// Initializes applicable systems, and adds them to the current SystemManager.
        /// </summary>
        private void InitializeSystems()
        {
            ILogger[] loggers = this._loggers.ToArray();
            this.SystemManager.Add(new PrototypeSystem(PROTOTYPES_FILE_PATH, this.SystemManager, loggers));

            this.SystemManager.Add(new RenderSystem(this.SystemManager, this._spriteBatch, loggers));
            this.SystemManager.Add(new GravitySystem(this.SystemManager, loggers));
            this.SystemManager.Add(new PhysicsSystem(this.SystemManager, loggers));
            this.SystemManager.Add(new KeymapSystem(this.SystemManager, loggers));
            this.SystemManager.Add(new MovementIntentSystem(this.SystemManager, loggers));
            this.SystemManager.Add(new CollisionSystem(this.SystemManager, loggers));
            //this.SystemManager.Add(new BlockingSystem(this.SystemManager, loggers));
            this.SystemManager.Add(new IntersectionSystem(this.SystemManager, loggers));
        }

        /// <summary>
        /// Initializes beginning entities and components, and adds them to the current EntityManager.
        /// </summary>
        private void InitializeEntities()
        {
            var playerComponents = new EcsComponent[] { new PhysicalComponent(this.EntityManager, new Vector2(50, 50)),
                                                        new RenderableComponent(this.EntityManager, this._playerTexture),
                                                        new MovableComponent(this.EntityManager, Vector2.Zero),
                                                        new MassiveComponent(this.EntityManager, 1),
                                                        new MovementIntentsComponent(this.EntityManager),
                                                        new CollideableComponent(this.EntityManager, (this._playerTexture.Width, this._playerTexture.Height)),
                                                        new SolidComponent(this.EntityManager), };
            this.EntityManager.AddEntity(playerComponents);

            var tileComponents = new EcsComponent[] { new PhysicalComponent(this.EntityManager, new Vector2(50, 200)),
                                                      new RenderableComponent(this.EntityManager, this._grassFloorTexture),
                                                      new CollideableComponent(this.EntityManager, (this._grassFloorTexture.Width, this._grassFloorTexture.Height)),
                                                      new SolidComponent(this.EntityManager), };
            this.EntityManager.AddEntity(tileComponents);
        }

        private void DrawTextWithShadow(string text, Vector2 position)
        {
            this._spriteBatch.Begin();
            this._spriteBatch.DrawString(this._uiFont, text, position + Vector2.One, Color.Black);
            this._spriteBatch.DrawString(this._uiFont, text, position, Color.White);
            this._spriteBatch.End();
        }

        #region Overrides definition.
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            GraphicsDevice.Clear(Color.White);

            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            this.EntityManager = new EntityManager(this);
            this.SystemManager = new SystemManager(this);

            this._uiFont = Content.Load<SpriteFont>("UiFont");

            this.InitializeLoggers();

            try
            {
                this.LoadAssets();
            }
            catch (ContentLoadException e)
            {
                DateTime time = DateTime.UtcNow;
                string errorText = e.Message;

                foreach (ILogger logger in this._loggers)
                {
                    logger.LogError(errorText, time);
                }

                this.Exit();
                return;
            }
            this.InitializeSystems();
            this.InitializeEntities();

            MediaPlayer.Play(this._songCollection[0]);

            //PrototypeSystem ptSystem = new PrototypeSystem("./entityPrototypes.json", this.SystemManager, this._loggers.ToArray());
            //ptSystem.BuildPrototypes();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

            foreach (IDisposable logger in this._loggers)
            {
                logger.Dispose();
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            this.SystemManager.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
        #endregion Overrides definition.
        #endregion Methods definition
    }
}
