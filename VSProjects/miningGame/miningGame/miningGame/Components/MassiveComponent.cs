﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that allows an entity to be affected by gravity.
    /// </summary>
    public class MassiveComponent : EcsComponent
    {
        #region Members definition
        public float Mass { get; set; }
        #endregion Members definition

        #region Constructors definition
        public MassiveComponent(EntityManager parent, float mass)
            : base(parent)
        {
            this.Mass = mass;
        }
        #endregion Constructors definition
    }
}
