﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the base class for all components.
    /// </summary>
    public abstract class EcsComponent
    {
        #region Members definition
        public EntityManager Parent;
        public int EntityID { get; set; }
        #endregion Members definition

        #region Constructors definition
        public EcsComponent(EntityManager parent)
        {
            this.Parent = parent;
        }

        public EcsComponent(EntityManager parent, int entityId)
            : this(parent)
        {
            this.EntityID = entityId;
        }
        #endregion Constructors definition
    }
}
