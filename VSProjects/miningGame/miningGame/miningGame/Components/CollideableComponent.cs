﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that allows an entity to collide with another entity.
    /// </summary>
    public class CollideableComponent : EcsComponent
    {
        #region Members definition
        public (int Width, int Height) BoundingBox { get; set; }
        #endregion Members definition

        #region Constructors definition
        public CollideableComponent(EntityManager parent, (int width, int height) bounds)
            : base(parent)
        {
            this.BoundingBox = bounds;
        }
        #endregion Constructors definition
    }
}
