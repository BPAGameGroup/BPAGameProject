﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that allows an entity to be rendered graphically.
    /// </summary>
    public class RenderableComponent : EcsComponent
    {
        #region Members definition
        public Texture2D Texture { get; set; }
        #endregion Members definition

        #region Constructors definition
        public RenderableComponent(EntityManager parent, Texture2D texture)
            : base(parent)
        {
            this.Texture = texture;
        }
        #endregion Constructors definition
    }
}
