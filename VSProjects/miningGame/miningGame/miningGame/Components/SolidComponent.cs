﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that marks an entity to prevent it from intersecting with other entities.
    /// </summary>
    public class SolidComponent : EcsComponent
    {
        #region Constructors definition
        public SolidComponent(EntityManager parent)
            : base(parent)
        { }
        #endregion Constructors definition
    }
}
