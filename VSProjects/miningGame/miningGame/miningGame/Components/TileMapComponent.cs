﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that defines the tile map for a game world.
    /// </summary>
    public class TileMapComponent : EcsComponent
    {
        #region Members definition
        public int[,] TileMap { get; set; }
        #endregion Members definition

        #region Constructors definition
        public TileMapComponent(EntityManager parent, int width, int height)
            : base(parent)
        {
            this.TileMap = new int[width, height];
        }
        #endregion Constructors definition
    }
}
