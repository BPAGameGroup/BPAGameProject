﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that allows an entity to move.
    /// </summary>
    public class MovableComponent : EcsComponent
    {
        #region Members definition
        public Vector2 Velocity { get; set; }
        #endregion Members definition

        #region Constructors definition
        public MovableComponent(EntityManager parent, Vector2 velocity)
            : base(parent)
        {
            this.Velocity = velocity;
        }
        #endregion Constructors definition
    }
}
