﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that allows an entity to react to movement intents.
    /// </summary>
    public class MovementIntentsComponent : EcsComponent
    {
        #region Members definition
        public bool LeftIntent { get; set; }
        public bool RightIntent { get; set; }
        public bool JumpIntent { get; set; }
        #endregion Members definition

        #region Constructors definition
        public MovementIntentsComponent(EntityManager parent)
            : base(parent)
        {
            this.ResetIntents();
        }
        #endregion Constructors definition

        #region Methods definition
        public void ResetIntents()
        {
            this.LeftIntent = false;
            this.RightIntent = false;
            this.JumpIntent = false;
        }
        #endregion Methods definition
    }
}
