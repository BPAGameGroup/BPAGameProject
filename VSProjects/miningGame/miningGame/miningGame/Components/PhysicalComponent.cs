﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame.Components
{
    /// <summary>
    /// Represents the data that allows an entity to physically exist in a game world.
    /// </summary>
    public class PhysicalComponent : EcsComponent
    {
        #region Members definition
        public Vector2 Position { get; set; }
        #endregion Members definition

        #region Methods definition
        public PhysicalComponent(EntityManager parent, Vector2 position)
            : base(parent)
        {
            this.Position = position;
        }
        #endregion Methods definition
    }
}
