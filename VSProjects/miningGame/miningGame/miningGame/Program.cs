using System;

namespace miningGame
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (MiningGame game = new MiningGame())
            {
                game.Run();
            }
        }
    }
#endif
}

