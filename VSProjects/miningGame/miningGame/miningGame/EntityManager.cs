﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using miningGame.Components;

namespace miningGame
{
    /// <summary>
    /// Represents a manager for ECS entities and ECS components.
    /// </summary>
    public class EntityManager
    {
        #region Members definition
        public MiningGame Parent { get; set; }
        public List<EcsComponent> Components = new List<EcsComponent>();
        #endregion Members definition

        #region Constructors definition
        public EntityManager(MiningGame parent)
        {
            this.Parent = parent;
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Adds the given component to this manager's list of components.
        /// </summary>
        /// <param name="component">The component to add.</param>
        public void AddComponent(EcsComponent component)
        {
            component.Parent = this;
            this.Components.Add(component);
        }

        /// <summary>
        /// Removes the given component from this manager's list of components.
        /// </summary>
        /// <param name="component">The component to remove.</param>
        public void RemoveComponent(EcsComponent component)
        {
            component.Parent = null;
            this.Components.Remove(component);
        }

        /// <summary>
        /// Adds the given list of components (representing an entity) to this manager's list.
        /// </summary>
        /// <param name="components">List of components to add.</param>
        public void AddEntity(params EcsComponent[] components)
        {
            int id;
            if (this.Components.Count == 0)
                { id = 0; }
            else
                { id = this.Components.Max(component => component.EntityID) + 1; }

            foreach (EcsComponent component in components)
            {
                component.EntityID = id;
                this.Components.Add(component);
            }
        }

        /// <summary>
        /// Gets the set of components tied to the given entity by ID.
        /// </summary>
        /// <param name="id">ID of the entity to get the components of.</param>
        /// <returns>Array of that entity's components.</returns>
        /// <exception cref="ArgumentException">Thrown if the given ID does not map to an existing entity.</exception>
        public EcsComponent[] GetEntity(int id)
        {
            if (id < 0)
                { throw new ArgumentException("Given ID does not map to a valid entity.", nameof(id)); }

            IEnumerable<EcsComponent> components = this.Components.Where(comp => comp.EntityID == id);

            if (!components.Any())
                { throw new ArgumentException("Given ID does not map to a valid entity.", nameof(id)); }

            return components.ToArray();
        }
        #endregion Methods definition
    }
}
