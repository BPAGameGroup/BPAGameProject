﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace miningGame
{
    /// <summary>
    /// Represents a logger that logs to the filesystem.
    /// </summary>
    public class FileLogger : ILogger, IDisposable
    {
        #region Members definition
        private readonly string _logFilePath;
        private readonly FileStream _logStream;
        private readonly StreamWriter _logStreamWriter;
        #endregion Members definition

        #region Constructors definition
        public FileLogger(string path)
        {
            this._logFilePath = path;
            this._logStream = new FileStream(this._logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            this._logStreamWriter = new StreamWriter(this._logStream);
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Dispose of all resources used by this logger.
        /// </summary>
        public void Dispose()
        {
            this._logStreamWriter.Close();
            this._logStreamWriter.Dispose();

            this._logStream.Close();
            this._logStream.Dispose();
        }

        /// <summary>
        /// Logs the given text to the filesystem.
        /// </summary>
        /// <param name="text">The text to log.</param>
        public void Log(string text)
        {
            this.Log(text, DateTime.UtcNow);
        }
        
        /// <summary>
        /// Logs the given text to the filesystem.
        /// </summary>
        /// <param name="text">The text to log.</param>
        /// <param name="time">The time to mark the log entry with.</param>
        public void Log(string text, DateTime time)
        {
            string timeString = time.ToString("yyyy-MM-dd HH:mm:ss");

            this._logStreamWriter.WriteLine(String.Format("[NORM] {0} - {1}", timeString, text));
        }

        // TODO: LogError should also log to a dedicated error log file, as well as the standard file.
        /// <summary>
        /// Logs the given text to the filesystem as an error.
        /// </summary>
        /// <param name="text">The text to log.</param>
        public void LogError(string text)
        {
            this.LogError(text, DateTime.UtcNow);
        }

        /// <summary>
        /// Logs the given text to the filesystem as an error.
        /// </summary>
        /// <param name="text">The text to log.</param>
        /// <param name="time">The time to mark the log entry with.</param>
        public void LogError(string text, DateTime time)
        {
            string timeString = time.ToString("yyyy-MM-dd HH:mm:ss");

            this._logStreamWriter.WriteLine(String.Format("[ERROR] {0} - {1}", timeString, text));
        }
        #endregion Methods definition
    }
}
