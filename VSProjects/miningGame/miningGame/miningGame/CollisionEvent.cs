﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame
{
    /// <summary>
    /// Represents a collision between two entities.
    /// </summary>
    public struct CollisionEvent
    {
        #region Members definition
        public int Entity1Id { get; set; }
        public int Entity2Id { get; set; }
        #endregion Members definition

        #region Constructors definition
        public CollisionEvent(int e1Id, int e2Id)
        {
            this.Entity1Id = e1Id;
            this.Entity2Id = e2Id;
        }
        #endregion Constructors definition
    }
}
