﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miningGame
{
    /// <summary>
    /// Represents a logger class that does not do anything when invoked.
    /// </summary>
    public class DummyLogger : ILogger
    {
        #region Methods definition
        public void Log(string text)
        {
            
        }

        public void Log(string text, DateTime time)
        {
            
        }

        public void LogError(string text)
        {
            
        }

        public void LogError(string text, DateTime time)
        {
            
        }
        #endregion Methods definition
    }
}
