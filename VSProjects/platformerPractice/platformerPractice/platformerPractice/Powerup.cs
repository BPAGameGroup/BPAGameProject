﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace platformerPractice
{
    public class Powerup : Sprite
    {
        #region Constructors definition
        public Powerup(Texture2D texture, Vector2 position, SpriteBatch batch)
            : base(texture, position, batch)
        { }
        #endregion Constructors definition

        #region Methods definition
        public override void Draw()
        {
            base.Draw();
        }
        #endregion Methods definition
    }
}
