﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace platformerPractice
{
    public struct MovementWrapper
    {
        #region Members definition
        public Vector2 MovementAttempt { get; private set; }
        public Vector2 CurrentPosition { get; set; }
        public int MovementStepCount { get; private set; }
        public bool IsDiagonal { get; private set; }
        public Vector2 MovementStep { get; private set; }
        public Rectangle Bounds { get; set; }
        #endregion Members definition

        #region Constructors definition
        public MovementWrapper(Vector2 original, Vector2 destination, Rectangle bounds) : this()
        {
            this.MovementAttempt = destination - original;
            this.CurrentPosition = original;
            this.MovementStepCount = (int)(this.MovementAttempt.Length() * 2) + 1;
            this.IsDiagonal = this.MovementAttempt.X != 0 && this.MovementAttempt.Y != 0;
            this.MovementStep = this.MovementAttempt / this.MovementStepCount;
            this.Bounds = bounds;
        }
        #endregion Constructors definition
    }
}
