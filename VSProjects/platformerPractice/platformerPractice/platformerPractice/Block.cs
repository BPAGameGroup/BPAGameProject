﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace platformerPractice
{
    public class Block : Sprite
    {
        #region Members definition
        public bool IsBlocked { get; set; }
        #endregion Members definition

        #region Constructors definition
        public Block(Texture2D texture, Vector2 position, SpriteBatch batch, bool isBlocked)
            : base(texture, position, batch)
        {
            this.IsBlocked = isBlocked;
        }
        #endregion Constructors definition

        #region Methods definition
        public override void Draw()
        {
            if (this.IsBlocked)
                { base.Draw(); }
        }
        #endregion Methods definition
    }
}
