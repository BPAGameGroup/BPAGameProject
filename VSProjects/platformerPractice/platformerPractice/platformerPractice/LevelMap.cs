﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace platformerPractice
{
    public class LevelMap
    {
        #region Members definition
        #region Fields definition
        private const double BLOCKED_CHANCE = 0.2;
        private const double POWERUP_CHANCE = 0.05;

        private Random _random = new Random();
        #endregion Fields definition

        #region Properties definition
        public static LevelMap CurrentLevelMap { get; private set; }

        public Block[,] Blocks { get; set; }
        public List<Powerup> Powerups { get; set; }
        public int Columns { get; set; }
        public int Rows { get; set; }
        public Texture2D BlockTexture { get; set; }
        public Texture2D PowerupTexture { get; set; }

        private SpriteBatch _batch { get; set; }
        #endregion Properties definition
        #endregion Members definition

        #region Constructors definition
        public LevelMap(SpriteBatch batch, Texture2D blockTexture, Texture2D powerupTexture, int columns, int rows)
        {
            this.Columns = columns;
            this.Rows = rows;
            this.BlockTexture = blockTexture;
            this.PowerupTexture = powerupTexture;
            this._batch = batch;
            this.Blocks = new Block[this.Columns, this.Rows];
            this.Powerups = new List<Powerup>();
            this.InitializeLevelMap();

            LevelMap.CurrentLevelMap = this;
        }
        #endregion Constructors definition

        #region Methods definition
        public void InitializeLevelMap()
        {
            this.InitializeTiles(BLOCKED_CHANCE, POWERUP_CHANCE);
        }

        private void InitializeTiles(double blockedChance, double powerupChance)
        {
            for (int x = 0; x < this.Columns; x++)
            {
                for (int y = 0; y < this.Rows; y++)
                {
                    // TODO: This first section places powerups. This is a garbage solution. Don't use this in the actual game.
                    bool placedPowerup = false;
                    if (!this.IsBorderTile(x, y) && this._random.NextDouble() < POWERUP_CHANCE)
                    {
                        placedPowerup = true;
                        Vector2 powerupPosition = new Vector2(x * this.PowerupTexture.Width, y * this.PowerupTexture.Height);
                        this.Powerups.Add(new Powerup(this.PowerupTexture, powerupPosition, this._batch));
                    }

                    bool isBlocked = false;
                    if (this.IsBorderTile(x, y) || (this._random.NextDouble() < blockedChance && !placedPowerup))
                        { isBlocked = true; }

                    Vector2 blockPosition = new Vector2(x * this.BlockTexture.Width, y * this.BlockTexture.Height);
                    this.Blocks[x, y] = new Block(this.BlockTexture, blockPosition, this._batch, isBlocked);
                }
            }

            // Set top left block to unblocked to make sure player does not get stuck.
            this.Blocks[1, 1].IsBlocked = false;
        }

        private bool IsBorderTile(int x, int y)
        {
            return (x == 0 || x == (this.Columns - 1) || y == 0 || y == (this.Rows - 1));
        }

        public void Draw()
        {
            foreach (Block block in this.Blocks)
            {
                block.Draw();
            }

            foreach (Powerup pup in this.Powerups)
            {
                pup.Draw();
            }
        }

        public bool HasRoomForRectangle(Rectangle rect)
        {
            foreach (Block block in this.Blocks)
            {
                if (block.IsBlocked && block.Bounds.Intersects(rect))
                {
                    return false;
                }
            }

            return true;
        }

        public Vector2 FindValidPosition(Vector2 original, Vector2 destination, Rectangle bounds)
        {
            MovementWrapper wrapper = new MovementWrapper(original, destination, bounds);

            for (int i = 0; i < wrapper.MovementStepCount; i++)
            {
                Vector2 positionAttempt = original + wrapper.MovementStep * i;
                Rectangle newBound = this.CreateRectangleAtPosition(positionAttempt, bounds.Width, bounds.Height);
                if (HasRoomForRectangle(newBound))
                    { wrapper.CurrentPosition = positionAttempt; }
                else
                {
                    // TODO: FindNonDiagonalMovement already checks wrapper.IsDiagonal. Is it safe to remove the double check?
                    if (wrapper.IsDiagonal)
                    {
                        wrapper.CurrentPosition = FindNonDiagonalMovement(wrapper, i);
                    }
                    break;
                }
            }

            return wrapper.CurrentPosition;
        }

        private Rectangle CreateRectangleAtPosition(Vector2 positionAttempt, int width, int height)
        {
            return new Rectangle((int)positionAttempt.X, (int)positionAttempt.Y, width, height);
        }

        private Vector2 FindNonDiagonalMovement(MovementWrapper wrapper, int i)
        {
            if (wrapper.IsDiagonal)
            {
                int stepsLeft = wrapper.MovementStepCount - (i - 1);

                Vector2 horizontalMovementRemaining = wrapper.MovementStep.X * Vector2.UnitX * stepsLeft;
                wrapper.CurrentPosition = FindValidPosition(wrapper.CurrentPosition, wrapper.CurrentPosition + horizontalMovementRemaining, wrapper.Bounds);

                Vector2 verticalMovementRemaining = wrapper.MovementStep.Y * Vector2.UnitY * stepsLeft;
                wrapper.CurrentPosition = FindValidPosition(wrapper.CurrentPosition, wrapper.CurrentPosition + verticalMovementRemaining, wrapper.Bounds);
            }

            return wrapper.CurrentPosition;
        }
        #endregion Methods definition
    }
}
