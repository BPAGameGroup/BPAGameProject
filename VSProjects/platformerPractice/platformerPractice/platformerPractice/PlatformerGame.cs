using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace platformerPractice
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class PlatformerGame : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Texture2D _blockTexture, _powerupTexture, _playerTexture;
        private Player _player;
        private LevelMap _levelMap;
        private SpriteFont _debugFont;
        private int _powerupGrabs;

        private Random _random = new Random();

        public PlatformerGame()
        {
            this._graphics = new GraphicsDeviceManager(this);
            this.Content.RootDirectory = "Content";
            this._graphics.PreferredBackBufferWidth = 960;
            this._graphics.PreferredBackBufferHeight = 640;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this._powerupGrabs = 0;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this._spriteBatch = new SpriteBatch(GraphicsDevice);

            this._blockTexture = this.Content.Load<Texture2D>("block");
            this._powerupTexture = this.Content.Load<Texture2D>("powerup");
            this._playerTexture = this.Content.Load<Texture2D>("player");
            this._player = new Player(this._playerTexture, new Vector2(80, 80), this._spriteBatch);
            this._levelMap = new LevelMap(this._spriteBatch, this._blockTexture, this._powerupTexture, 15, 10);
            this._debugFont = Content.Load<SpriteFont>("DebugFont");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        private int collision = 0;

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            this._player.Update(gameTime);
            //collision++;
            //if (collision == 50)
            //{
            //    collision = 0;
            //    this.CheckPowerupContact();
            //}
            this.CheckPowerupContact();

            this.RespondToKeyboard();

            base.Update(gameTime);
        }

        private void RespondToKeyboard()
        {
            KeyboardState state = Keyboard.GetState();

            if (state.IsKeyDown(Keys.F5)) { this.RestartGame(); }
            if (state.IsKeyDown(Keys.Escape)) { this.Exit(); }
        }

        private void RestartGame()
        {
            LevelMap.CurrentLevelMap.InitializeLevelMap();
            this.SpawnPlayer();
            this._powerupGrabs = 0;
        }

        private void SpawnPlayer()
        {
            this._player.Position = Vector2.One * 80;
            this._player.Movement = Vector2.Zero;
        }

        private void CheckPowerupContact()
        {
            Vector2 playerTopLeft = this._player.Position;
            Vector2 playerBottomRight = new Vector2(playerTopLeft.X + this._player.Bounds.Width, playerTopLeft.Y + this._player.Bounds.Height);

            for (int i = this._levelMap.Powerups.Count - 1; i >= 0; i--)
            {
                Powerup pup = this._levelMap.Powerups[i];

                Vector2 pupTopLeft = pup.Position;
                Vector2 pupBottomRight = new Vector2(pupTopLeft.X + pup.Texture.Bounds.Width, pupTopLeft.Y + pup.Texture.Bounds.Height);

                if (IsColliding(playerTopLeft, playerBottomRight, pupTopLeft, pupBottomRight))
                {
                    this._levelMap.Powerups.Remove(pup);
                    this._powerupGrabs++;
                    continue;
                }
            }
        }

        // TODO: This should probably be optimized, given how often it's called.
        private bool IsColliding(Vector2 aTopLeft, Vector2 aBotRight, Vector2 bTopLeft, Vector2 bBotRight)
        {
            // Extract individual values from vectors
            float atlx = aTopLeft.X;
            float atly = aTopLeft.Y;

            float abrx = aBotRight.X;
            float abry = aBotRight.Y;

            float btlx = bTopLeft.X;
            float btly = bTopLeft.Y;

            float bbrx = bBotRight.X;
            float bbry = bBotRight.Y;

            // 8 positive comparisons
            bool atlxToBtlx = atlx > btlx;
            bool atlxToBbrx = atlx > bbrx;

            bool atlyToBtly = atly > btly;
            bool atlyToBbry = atly > bbry;

            bool abrxToBtlx = abrx > btlx;
            bool abrxToBbrx = abrx > bbrx;

            bool abryToBtly = abry > btly;
            bool abryToBbry = abry > bbry;

            // XORs each pair of comparisons to determine if each corner is between the corners of the other box on each axis.
            // Example: atlxInside determines if the top left corner of object A is between the corners of object B on the X axis.
            bool atlxInside = atlxToBtlx != atlxToBbrx;

            bool atlyInside = atlyToBtly != atlyToBbry;

            bool abrxInside = abrxToBtlx != abrxToBbrx;

            bool abryInside = abryToBtly != abryToBbry;

            // ANDs each pair of between comparisons to determine if each corner of object A is inside object A.
            // Example: If the top left corner of object A is inside object B on both the X axis and the Y axis, that corner of object A is inside object B.
            bool atlInside = atlxInside && atlyInside;

            bool abrInside = abrxInside && abryInside;

            // If either corner or both of object A is inside object B, then they are colliding.
            return atlInside || abrInside;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            this._spriteBatch.Begin();
            base.Draw(gameTime);
            this._levelMap.Draw();
#if DEBUG
            this.WriteDebugInformation();
#endif
            this._player.Draw();
            this._spriteBatch.End();

            base.Draw(gameTime);
        }

        private void WriteDebugInformation()
        {
            string positionText = string.Format("Player position: ({0:0.0}, {1:0.0})", this._player.Position.X, this._player.Position.Y);
            string movementText = string.Format("Player movement: ({0:0.0}, {1:0.0})", this._player.Movement.X, this._player.Movement.Y);
            string firmGroundText = string.Format("On firm ground: {0}", this._player.IsOnFirmGround());
            string powerupText = string.Format("Number of powerups collected: {0}", this._powerupGrabs.ToString());

            this.DrawTextWithShadow(positionText, new Vector2(10, 0));
            this.DrawTextWithShadow(movementText, new Vector2(10, 20));
            this.DrawTextWithShadow(firmGroundText, new Vector2(10, 40));
            this.DrawTextWithShadow(powerupText, new Vector2(10, 60));
        }

        private void DrawTextWithShadow(string text, Vector2 position)
        {
            this._spriteBatch.DrawString(this._debugFont, text, position + Vector2.One, Color.Black);
            this._spriteBatch.DrawString(this._debugFont, text, position, Color.White);
        }
    }
}
