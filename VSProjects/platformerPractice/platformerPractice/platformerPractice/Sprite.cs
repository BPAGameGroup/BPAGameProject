﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace platformerPractice
{
    public class Sprite
    {
        #region Members definition
        public Vector2 Position { get; set; }
        public Texture2D Texture { get; set; }
        public SpriteBatch SpriteBatch { get; set; }

        public Rectangle Bounds
        {
            get { return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.Texture.Width, this.Texture.Height); }
        }
        #endregion Members definition

        #region Constructors definition
        public Sprite(Texture2D texture, Vector2 position, SpriteBatch batch)
        {
            this.Texture = texture;
            this.Position = position;
            this.SpriteBatch = batch;
        }
        #endregion Constructors definition

        #region Methods definition
        public virtual void Draw()
        {
            this.SpriteBatch.Draw(this.Texture, this.Position, Color.White);
        }
        #endregion Methods definition
    }
}
