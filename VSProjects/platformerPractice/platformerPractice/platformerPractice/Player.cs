﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace platformerPractice
{
    public class Player : Sprite
    {
        #region Members definition
        #region Fields definition
        private Vector2 _oldPosition;
        #endregion Fields definition

        #region Properties definition
        public Vector2 Movement { get; set; }
        #endregion Properties definition
        #endregion Members definition

        #region Constructors definition
        public Player(Texture2D texture, Vector2 position, SpriteBatch batch)
            : base(texture, position, batch)
        { }
        #endregion Constructors definition

        #region Methods definition
        public void Update(GameTime gameTime)
        {
            this.DetermineMovement();
            this.ApplyMovement(gameTime);
            this.ApplyBlocking();
        }

        private void DetermineMovement()
        {
            this.ApplyKeyboardMovement();
            this.ApplyGravity();
            this.ApplyFriction();
        }

        private void ApplyKeyboardMovement()
        {
            KeyboardState state = Keyboard.GetState();

            // TODO: Why is left movement smaller than right movement?
            if (state.IsKeyDown(Keys.Left)) { this.Movement -= Vector2.UnitX * 0.5f; }
            if (state.IsKeyDown(Keys.Right)) { this.Movement += Vector2.UnitX; }
            if (state.IsKeyDown(Keys.Space) && this.IsOnFirmGround()) { this.Movement = -Vector2.UnitY * 20; }
        }

        private void ApplyGravity()
        {
            this.Movement += Vector2.UnitY * 0.65f;
        }

        private void ApplyFriction()
        {
            if (this.IsOnFirmGround())
                { this.Movement -= this.Movement * Vector2.One * 0.08f; }
            else
                { this.Movement -= this.Movement * Vector2.One * 0.02f; }
        }

        private void ApplyMovement(GameTime gameTime)
        {
            this._oldPosition = this.Position;
            this.UpdatePosition(gameTime);
            this.Position = LevelMap.CurrentLevelMap.FindValidPosition(this._oldPosition, this.Position, this.Bounds);
        }

        private void UpdatePosition(GameTime gameTime)
        {
            this.Position += this.Movement * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 15;
        }

        private void ApplyBlocking()
        {
            Vector2 lastMovement = this.Position - this._oldPosition;
            if (lastMovement.X == 0) { this.Movement *= Vector2.UnitY; }
            if (lastMovement.Y == 0) { this.Movement *= Vector2.UnitX; }
        }

        public bool IsOnFirmGround()
        {
            Rectangle below = this.Bounds;
            below.Offset(0, 1);
            return !LevelMap.CurrentLevelMap.HasRoomForRectangle(below);
        }
        #endregion Methods definition
    }
}
