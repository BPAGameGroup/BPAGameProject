﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace architecturePractice
{
    public abstract class GameObject
    {
        #region Members definition
        public Vector2 Position { get; set; }
        public Texture2D Texture { get; set; }
        public SpriteBatch SpriteBatch { get; set; }

        public Rectangle BoundingBox
        {
            get { return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.Texture.Width, this.Texture.Height); }
        }

        public (Vector2, Vector2) BoundsCorners
        {
            get { return this.GetBoundingBoxCorners(); }
        }
        #endregion Members definition

        #region Constructors definition
        public GameObject(Texture2D texture, Vector2 position, SpriteBatch batch)
        {
            this.Texture = texture;
            this.Position = position;
            this.SpriteBatch = batch;
        }
        #endregion Constructors definition

        #region Methods definition
        /// <summary>
        /// Gets a tuple containing two vectors: first, the top left corner of this object's bounding box, second, the bottom right corner of this object's bounding box.
        /// </summary>
        /// <returns>A tuple containing the top left and bottom right corners of this object's bounding box.</returns>
        private (Vector2, Vector2) GetBoundingBoxCorners()
        {
            Vector2 topLeft = this.Position;
            Vector2 bottomRight = new Vector2(topLeft.X + this.Texture.Width, topLeft.Y + this.Texture.Height);

            return (topLeft, bottomRight);
        }

        public virtual void Draw()
        {
            this.SpriteBatch.Draw(this.Texture, this.Position, Color.White);
        }
        #endregion Methods definition
    }
}
