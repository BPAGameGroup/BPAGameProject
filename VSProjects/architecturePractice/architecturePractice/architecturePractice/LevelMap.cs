﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace architecturePractice
{
    public class LevelMap
    {
        #region Members definition
        #region Fields definition
        private readonly Random _random;
        #endregion Fields definition

        #region Properties definition
        public WorldTile[,] PhysicalMap { get; set; }
        public int Columns
        {
            get { return this.PhysicalMap.GetLength(1); }
        }
        public int Rows
        {
            get { return this.PhysicalMap.GetLength(0); }
        }

        
        #endregion Properties definition
        #endregion Members definition

        #region Constructors definition
        public LevelMap(Random random, int columns, int rows)
        {
            this._random = random;

            this.PhysicalMap = new WorldTile[columns, rows];
        }
        #endregion Constructors definition
    }
}
