﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace architecturePractice
{
    public class Player : GameObject, IMovable
    {
        #region Members definition
        #region Fields definition

        #endregion Fields definition

        #region Properties definition
        public Vector2 Movement { get; set; }
        #endregion Properties definition
        #endregion Members definition

        #region Constructors definition
        public Player(Texture2D texture, Vector2 position, SpriteBatch batch)
            : base(texture, position, batch)
        { }
        #endregion Constructors definition

        #region Methods definition
        public void Update(GameTime time)
        {
            throw new NotImplementedException();
        }
        #endregion Methods definition
    }
}
