using System;

namespace architecturePractice
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (ArcPracticeGame game = new ArcPracticeGame())
            {
                game.Run();
            }
        }
    }
#endif
}

