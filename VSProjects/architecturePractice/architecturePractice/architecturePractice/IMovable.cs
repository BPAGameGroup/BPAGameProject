﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace architecturePractice
{
    public interface IMovable
    {
        Vector2 Movement { get; set; }

        void Update(GameTime time);
    }
}
