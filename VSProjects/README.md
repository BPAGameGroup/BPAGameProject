# Visual Studio Projects Folder

This folder is to be used for all Visual Studio projects used in development, including the final game project. It will also include any test/sample projects used in development for practice or experimentation.