# Design Folder

This folder is to be used for all design documents of the project. While most work in design will be done on Google Docs to use the collaborative features, copies should be kept here as well.